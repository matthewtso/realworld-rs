# Rust Real World

A backend implementation of the API specification at [https://realworld.io/]().
Technologies used: Rust ([Nickel](https://nickel-org.github.io/)), etc.

## Development

Running with debug logging.

``` bash
make run-debug
```

Running debug binary.

``` bash
make run
```

## TODO

- Handle `.unwrap()`'s better.
- Implement articles.
- Implement comments.
