use super::errors::{
    InvalidInputError, NotFoundError, UnauthorizedError, UnexpectedError,
};
use nickel::{MiddlewareResult, Responder, Response};

use self::ProcessorErrorKind::{FollowNotFound, UserNotFound};
use processors::{ProcessorError, ProcessorErrorKind};

pub struct ErrorMapper<'mw, D: 'mw> {
    response: Response<'mw, D>,
}

impl<'mw, D> ErrorMapper<'mw, D> {
    pub fn builder(
        mut response: Response<'mw, D>,
    ) -> ErrorMapperBuilder<'mw, D> {
        ErrorMapperBuilder::builder(response)
    }

    pub fn new(mut response: Response<'mw, D>) -> Self {
        Self { response }
    }

    pub fn send(mut self, error: ProcessorError) -> MiddlewareResult<'mw, D> {
        self.send_target(error, None)
    }

    pub fn send_target(
        mut self,
        error: ProcessorError,
        target: Option<&str>,
    ) -> MiddlewareResult<'mw, D> {
        match error.kind() {
            FollowNotFound => self
                .response
                .send(NotFoundError::path(target.unwrap_or("follow"), None)), // Tried to delete a follow that wasn't there.
            _ => self.response.send(UnexpectedError::new(None)),
        }
    }
}

pub struct ErrorMapperBuilder<'mw, D: 'mw> {
    response: Response<'mw, D>,
    resource: Option<&'mw str>,
    target: Option<&'mw str>,
    id: Option<&'mw str>,
    reason: Option<&'mw str>,
}

impl<'mw, D> ErrorMapperBuilder<'mw, D> {
    pub fn builder(mut response: Response<'mw, D>) -> Self {
        Self {
            response,
            resource: None,
            target: None,
            id: None,
            reason: None,
        }
    }

    pub fn resource(mut self, resource: &'mw str) -> Self {
        self.resource = Some(resource);
        self
    }

    pub fn target(mut self, target: &'mw str) -> Self {
        self.target = Some(target);
        self
    }

    pub fn id(mut self, id: &'mw str) -> Self {
        self.id = Some(id);
        self
    }

    pub fn reason(mut self, reason: &'mw str) -> Self {
        self.reason = Some(reason);
        self
    }

    pub fn send(mut self, error: ProcessorError) -> MiddlewareResult<'mw, D> {
        match error.kind() {
            FollowNotFound => self.response.send(NotFoundError::path(
                self.target.unwrap_or("follow"),
                self.id, //.map_or(None, |id| Some(id.as_str())),
            )), // Tried to delete a follow that wasn't there.
            _ => self.response.send(UnexpectedError::new(self.reason)),
        }
    }
}
