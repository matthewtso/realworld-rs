use hyper::method::Method::{Delete, Get, Post};
use nickel::{Middleware, MiddlewareResult, Request, Response};

use super::error_mapper::ErrorMapper;
use super::errors::{
    InvalidInputError, MethodNotAllowedError, NotFoundError, UnexpectedError,
};
use super::JsonResponse;
use helpers::authentication::AuthenticationHelper;
use processors::{FollowProcessor, ProcessorErrorKind, ProfileProcessor};

#[derive(Debug, Clone)]
pub struct ProfileResource {
    profile_processor: ProfileProcessor,
    follow_processor: FollowProcessor,
    auth_helper: AuthenticationHelper,
}

impl<T> Middleware<T> for ProfileResource {
    fn invoke<'a>(
        &self,
        req: &mut Request<T>,
        res: Response<'a, T>,
    ) -> MiddlewareResult<'a, T> {
        match (
            &req.origin.method,
            req.param("username"),
            req.param("action"),
        ) {
            // /profiles/:username/login
            (Get, Some(_), None) => self.get_instance(req, res),

            // /profiles/:username/follow
            (Post, Some(_), Some("follow")) => {
                self.post_instance_follow(req, res)
            }
            (Delete, Some(_), Some("follow")) => {
                self.delete_instance_follow(req, res)
            }

            // self.post_instance(req, res), // Method not allowed error (not Not found because we already matched some route)
            _ => self.method_not_allowed(req, res),
        }
    }
}

impl ProfileResource {
    pub fn new(
        profile_processor: ProfileProcessor,
        follow_processor: FollowProcessor,
        auth_helper: AuthenticationHelper,
    ) -> Self {
        ProfileResource {
            profile_processor,
            follow_processor,
            auth_helper,
        }
    }

    pub fn method_not_allowed<'mw, D>(
        &self,
        req: &mut Request<D>,
        res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        res.send(MethodNotAllowedError::new(&format!(
            "{}",
            req.origin.method
        )))
    }

    pub fn get_instance<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let user_id = match self.auth_helper.get_auth_from_request(req) {
            Ok(claims) => Some(claims.user_id),
            Err(_) => None,
        };

        let username = if let Some(username) = req.param("username") {
            username
        } else {
            return res.send(InvalidInputError::path("username"));
        };

        match self.profile_processor.get_by_username(username, user_id) {
            Ok(profile) => JsonResponse::new(res).send_json(profile),
            Err(e) => match e.kind() {
                ProcessorErrorKind::UserNotFound(username) => res.send(
                    NotFoundError::path("users", Some(username.as_str())),
                ),
                _ => res.send(UnexpectedError::new(None)),
            },
        }
    }

    pub fn post_instance_follow<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let user_id = match self.auth_helper.get_auth_from_request(req) {
            Ok(claims) => claims.user_id,
            Err(e) => return res.send(e),
        };

        let username = match req.param("username") {
            Some(username) => username,
            None => return res.send(UnexpectedError::new(None)),
        };

        match self.follow_processor.create_follow(&user_id, username) {
            Ok(follow) => match self
                .profile_processor
                .get_by_username(username, Some(user_id))
            {
                Ok(profile) => JsonResponse::new(res).send_json(profile),
                Err(_) => res.send(UnexpectedError::new(None)), // TODO: Handle better.
            },
            Err(e) => match e.kind() {
                ProcessorErrorKind::UserNotFound(id) => {
                    if id == &user_id {
                        res.send(NotFoundError::token("users", None))
                    } else {
                        res.send(NotFoundError::path("users", None))
                    }
                }
                _ => res.send(UnexpectedError::new(None)),
            },
        }
    }

    pub fn delete_instance_follow<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let user_id = match self.auth_helper.get_auth_from_request(req) {
            Ok(claims) => claims.user_id,
            Err(e) => return res.send(e),
        };

        let username = match req.param("username") {
            Some(username) => username,
            None => return res.send(UnexpectedError::new(None)),
        };

        match self
            .profile_processor
            .delete_follow_returning_profile(&user_id, username)
        {
            Ok(profile) => JsonResponse::new(res).send_json(profile),
            Err(e) => ErrorMapper::builder(res)
                .resource("profile")
//                .id(username) // TODO: Resolve lifetimes?
                .send(e),
        }
    }
}
