use nickel::{MiddlewareResult, Responder, Response};
use serde_json::value::Value;

use self::ProcessorErrorKind::{FollowNotFound, UserNotFound};
use super::errors::{InvalidInputError, NotFoundError, UnexpectedError};
use processors::{ProcessorError, ProcessorErrorKind};

const CONTENT_TYPE: &'static str = "Content-Type";
const APPLICATION_JSON: &'static [u8] = b"application/json";

/// Conform a struct to ToJson to let it be passed to JsonResponse#send_json.
pub trait ToJson {
    fn to_json(&self) -> Value;
}

/// Helper container to handle the setting of JSON content type headers
/// and the serialization of ToJson to send-able data.
pub struct JsonResponse<'mw, D: 'mw> {
    response: Response<'mw, D>,
}

impl<'mw, D> JsonResponse<'mw, D> {
    pub fn new(mut response: Response<'mw, D>) -> Self {
        Self { response }
    }

    pub fn send(mut self, body: Value) -> MiddlewareResult<'mw, D> {
        self.append_json_header();
        self.response.send(body.to_string())
    }

    pub fn send_json<T>(mut self, body: T) -> MiddlewareResult<'mw, D>
    where
        T: ToJson + Sized,
    {
        self.append_json_header();
        self.response.send(body.to_json().to_string())
    }

    fn append_json_header(&mut self) {
        self.response
            .headers_mut()
            .append_raw(CONTENT_TYPE, APPLICATION_JSON.to_vec());
    }
}
