use super::{ProfileResource, UserResource};
use config::Configuration;
use helpers::authentication::AuthenticationHelper;
use processors::ProcessorFactory;

pub struct ResourceFactory {
    user_resource: UserResource,
    profile_resource: ProfileResource,
}

impl ResourceFactory {
    pub fn new(
        processor_factory: ProcessorFactory,
        configuration: &Configuration,
    ) -> Self {
        ResourceFactory {
            user_resource: UserResource::new(
                processor_factory.get_user_processor(),
                AuthenticationHelper::new(
                    configuration.application.token_secret.to_string(),
                ),
            ),
            profile_resource: ProfileResource::new(
                processor_factory.get_profile_processor(),
                processor_factory.get_follow_processor(),
                AuthenticationHelper::new(
                    configuration.application.token_secret.to_string(),
                ),
            ),
        }
    }

    pub fn get_user_resource(&self) -> UserResource {
        self.user_resource.clone()
    }

    pub fn get_profile_resource(&self) -> ProfileResource {
        self.profile_resource.clone()
    }
}
