use super::errors::InvalidInputError;
use nickel::Request;
use serde::de::DeserializeOwned;
use std::io::Read;

pub fn from_body<T, D>(req: &mut Request<D>) -> Result<T, InvalidInputError>
where
    T: DeserializeOwned,
{
    let mut body = String::new();
    req.origin.read_to_string(&mut body).unwrap();

    match serde_json::from_str::<T>(&body) {
        Ok(d) => Ok(d),
        Err(e) => Err(InvalidInputError::body(&format!("{}", e))),
    }
}

use super::errors::MethodNotAllowedError;
use nickel::{MiddlewareResult, Response};

pub fn method_not_allowed<'mw, D>(
    req: &mut Request<D>,
    res: Response<'mw, D>,
) -> MiddlewareResult<'mw, D> {
    res.send(MethodNotAllowedError::new(&format!(
        "{}",
        req.origin.method
    )))
}
