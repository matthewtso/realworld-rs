use hyper::status::StatusCode::{
    BadRequest, Forbidden, MethodNotAllowed, NotFound, UnprocessableEntity,
};
use nickel::{Action, ErrorHandler, Halt, NickelError, Request};
use std::io::Write;

#[derive(Clone, Copy)]
pub struct AppErrorHandler;

// TODO: Make this better, application errors should probably be handled in a middleware
//       and not here because the Response stream's header is immutable! So somehow the
//       Content-Type header must be first set in the error, then the error must respond
//       by calling `res.error(StatusCode, self)` to get to this mapper!
impl<D> ErrorHandler<D> for AppErrorHandler {
    fn handle_error(
        &self,
        err: &mut NickelError<D>,
        _req: &mut Request<D>,
    ) -> Action {
        if let Some(ref mut res) = err.stream {
            // Why doesn't this work??
            // res.headers_mut().append_raw("Content-Type", b"application/json; charset=utf-8".to_vec());

            let msg: &[u8] = match &res.status() {
                NotFound => if "File Not Found" == err.message {
                    r#"{"errors":{"path":["Not found"]}}"#.as_bytes()
                } else {
                    err.message.as_bytes()
                },
                Forbidden | BadRequest | UnprocessableEntity
                | MethodNotAllowed => err.message.as_bytes(),
                _ => r#"{"errors":{"server":["Internal server error"]}}"#
                    .as_bytes(),
            };

            let _ = res.write_all(msg);
        } else {
            println!("Error: {}", err.message);
        }

        Halt(())
    }
}
