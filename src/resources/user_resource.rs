use hyper::method::Method::{Get, Post, Put};
use nickel::{Middleware, MiddlewareResult, Request, Response};
use serde_json;
use serde_json::value::Value;
use std::collections::HashMap;
use std::error::Error;
use std::io::Read;
use std::sync::Arc;

use super::errors::{
    InvalidInputError, MethodNotAllowedError, NotFoundError, UnexpectedError,
};
use super::helpers::from_body;
use super::JsonResponse;
use helpers::authentication::AuthenticationHelper;
use processors::ProcessorErrorKind;
use processors::UserProcessor;

#[derive(Debug, Clone)]
pub struct UserResource {
    user_processor: UserProcessor,
    auth_helper: AuthenticationHelper,
}

impl<T> Middleware<T> for UserResource {
    fn invoke<'a>(
        &self,
        req: &mut Request<T>,
        res: Response<'a, T>,
    ) -> MiddlewareResult<'a, T> {
        match (
            &req.origin.method,
            req.param("path"),
            req.param("subresource"),
        ) {
            // /users/login
            (Post, Some("login"), None) => self.post_collection_login(req, res),
            (Get, Some(_), None) => self.get_instance(req, res),
            (Put, None, None) => self.put_instance(req, res),
            (Post, None, None) => self.post_collection(req, res),
            (Get, None, None) => self.get_instance(req, res),

            // self.post_instance(req, res), // Method not allowed error (not Not found because we already matched some route)
            _ => self.method_not_allowed(req, res),
        }
    }
}

#[derive(Debug, Deserialize)]
struct UserLoginInner {
    email: String,
    password: String,
}

#[derive(Debug, Deserialize)]
struct UserCreateInner {
    email: String,
    username: String,
    password: String,
    bio: Option<String>,
    image: Option<String>,
}

#[derive(Debug, Deserialize)]
struct UserRequest<T> {
    user: T,
}

impl UserResource {
    pub fn new(
        user_processor: UserProcessor,
        auth_helper: AuthenticationHelper,
    ) -> Self {
        UserResource {
            user_processor,
            auth_helper,
        }
    }

    pub fn method_not_allowed<'mw, D>(
        &self,
        req: &mut Request<D>,
        res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        res.send(MethodNotAllowedError::new(&format!(
            "{}",
            req.origin.method
        )))
    }

    pub fn post_collection<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let data: UserRequest<UserCreateInner> = match from_body(req) {
            Ok(d) => d,
            Err(e) => return res.send(e),
        };

        match self.user_processor.create(
            &data.user.email,
            &data.user.username,
            &data.user.password,
            if let Some(ref bio) = data.user.bio {
                Some(&bio)
            } else {
                None
            },
            if let Some(ref image) = data.user.image {
                Some(&image)
            } else {
                None
            },
        ) {
            Ok(user) => match self.auth_helper.encode_token(user.get_id()) {
                Ok(token) => {
                    JsonResponse::new(res).send(user.to_json_with_token(&token))
                }
                Err(e) => res.send(e),
            },
            Err(e) => match e.kind() {
                ProcessorErrorKind::UserAlreadyExists(_) => {
                    res.send(InvalidInputError::body(&format!(
                        "User {} already exists",
                        &data.user.email
                    )))
                }
                _ => res.send(UnexpectedError::new(Some(e.description()))),
            },
        }
    }

    pub fn put_instance<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let user_id = match self.auth_helper.get_auth_from_request(req) {
            Ok(claims) => claims.user_id,
            Err(e) => return res.send(e),
        };

        let mut body = String::new();
        req.origin.read_to_string(&mut body).unwrap();

        let request_data: UserRequest<Value> =
            serde_json::from_str(&body).unwrap();

        let mut fields: HashMap<String, Option<String>> = HashMap::new();

        if let Value::Object(object) = request_data.user {
            for (ref key, v) in object.iter() {
                match (key.as_str(), v) {
                    ("image", Value::String(image)) => {
                        fields.insert(key.to_string(), Some(image.to_string()))
                    }
                    ("image", Value::Null) => {
                        fields.insert(key.to_string(), None)
                    }
                    (k, Value::String(v))
                        if k == "email"
                            || k == "username"
                            || k == "bio"
                            || k == "password" =>
                    {
                        fields.insert(key.to_string(), Some(v.to_string()))
                    }
                    (&_, _) => None,
                };
            }
        } else {
            return res.send(InvalidInputError::body(&format!(
                "Missing 'user' object"
            )));
        }

        match self.user_processor.update(&user_id, fields) {
            Ok(user) => match self.auth_helper.encode_token(user.get_id()) {
                Ok(token) => {
                    JsonResponse::new(res).send(user.to_json_with_token(&token))
                }
                Err(e) => res.send(e),
            },
            Err(_) => res.send(UnexpectedError::new(None)),
            // TODO: handle this
            //            Err(e) if let ProcessorErrorKind::UserNotFound(_) = e.kind() { true } else { false } => res.send(NotFoundError::)
        }
    }

    pub fn get_instance<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let user_id = match self.auth_helper.get_auth_from_request(req) {
            Ok(claims) => claims.user_id,
            Err(e) => return res.send(e),
        };

        match self.user_processor.get_by_id(&user_id) {
            Ok(user) => match self.auth_helper.encode_token(user.get_id()) {
                Ok(token) => {
                    JsonResponse::new(res).send(user.to_json_with_token(&token))
                }
                Err(e) => res.send(e),
            },
            Err(e) => match e.kind() {
                ProcessorErrorKind::UserNotFound(_) => {
                    res.send(NotFoundError::token("users", Some(&user_id)))
                }
                _ => res.send(UnexpectedError::new(None)),
            },
        }
    }

    /// Login
    pub fn post_collection_login<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let data: UserRequest<UserLoginInner> = match from_body(req) {
            Ok(d) => d,
            Err(e) => return res.send(e),
        };

        if let Some(user) = self.user_processor.get_by_email(&data.user.email) {
            match (
                user.verify_password(&data.user.password),
                self.auth_helper.encode_token(user.get_id()),
            ) {
                (false, _) => {
                    res.send(InvalidInputError::body("Invalid password"))
                }
                (true, Ok(token)) => {
                    JsonResponse::new(res).send(user.to_json_with_token(&token))
                }
                (true, Err(e)) => res.send(e),
            }
        } else {
            res.send(InvalidInputError::body("User not found"))
        }
    }
}
