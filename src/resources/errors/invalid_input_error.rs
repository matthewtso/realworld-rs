use hyper::status::StatusCode::UnprocessableEntity;
use nickel::{MiddlewareResult, Responder, Response};
use std::borrow::Cow;
use std::error::Error;
use std::fmt::{self, Display, Formatter};

use super::ErrorResponse;

#[derive(Debug)]
pub struct InvalidInputError {
    message: String,
    target: String,
}

impl InvalidInputError {
    pub fn body(message: &str) -> Self {
        Self {
            message: format!("Invalid input: {}", message),
            target: "body".to_string(),
        }
    }

    pub fn path(message: &str) -> Self {
        Self {
            message: format!("Invalid input: {}", message),
            target: "path".to_string(),
        }
    }

    pub fn header(message: &str) -> Self {
        Self {
            message: format!("Invalid input: {}", message),
            target: "header".to_string(),
        }
    }
}

impl Display for InvalidInputError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for InvalidInputError {
    fn description(&self) -> &str {
        &self.message
    }
}

pub const INVALID_INPUT: &'static str = "INVALID_INPUT";

impl<'a> From<InvalidInputError> for Cow<'a, str> {
    fn from(err: InvalidInputError) -> Self {
        Cow::from(err.message)
    }
}

impl<D> Responder<D> for InvalidInputError {
    fn respond<'a>(self, res: Response<'a, D>) -> MiddlewareResult<'a, D> {
        res.send(ErrorResponse::new(
            UnprocessableEntity,
            INVALID_INPUT,
            &self.target,
            vec![self.description()],
        ))
    }
}
