use hyper::status::StatusCode::Forbidden;
use nickel::{MiddlewareResult, Responder, Response};
use std::borrow::Cow;
use std::error::Error;
use std::fmt::{self, Display, Formatter};

use super::ErrorResponse;

#[derive(Debug)]
pub struct UnauthorizedError {
    message: String,
}

impl UnauthorizedError {
    pub fn new(user_id: &str) -> Self {
        Self {
            message: format!("User {} is not permitted", user_id),
        }
    }
}

impl Display for UnauthorizedError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for UnauthorizedError {
    fn description(&self) -> &str {
        &self.message
    }
}

pub const UNAUTHORIZED: &'static str = "UNAUTHORIZED";

impl<'a> From<UnauthorizedError> for Cow<'a, str> {
    fn from(err: UnauthorizedError) -> Self {
        Cow::from(err.message)
    }
}

impl<D> Responder<D> for UnauthorizedError {
    fn respond<'a>(self, res: Response<'a, D>) -> MiddlewareResult<'a, D> {
        res.send(ErrorResponse::new(
            Forbidden,
            UNAUTHORIZED,
            "body",
            vec![self.description()],
        ))
    }
}
