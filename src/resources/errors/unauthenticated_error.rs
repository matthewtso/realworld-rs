use nickel::{MiddlewareResult, Responder, Response};
use std::error::Error;
use std::fmt::{self, Display, Formatter};

use super::ErrorResponse;

#[derive(Debug)]
pub struct UnauthenticatedError {
    message: String,
}

impl UnauthenticatedError {
    pub fn new() -> Self {
        Self {
            message: "Not authenticated".to_string(),
        }
    }
}

impl Display for UnauthenticatedError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for UnauthenticatedError {
    fn description(&self) -> &str {
        &self.message
    }
}

pub const UNAUTHENTICATED: &'static str = "UNAUTHENTICATED";

//impl<D> Responder<D> for UnauthenticatedError {
//    fn respond<'a>(self, res: mut Response<'a, D>) -> MiddlewareResult<'a, D> {
////        res.headers_mut().append_raw()
//        res.send(ErrorResponse {
//            error: UNAUTHENTICATED,
//            message: self.description(),
//        })
//    }
//}
