use hyper::status::StatusCode::MethodNotAllowed;
use nickel::{MiddlewareResult, Responder, Response};
use std::borrow::Cow;
use std::error::Error;
use std::fmt::{self, Display, Formatter};

use super::ErrorResponse;

pub const METHOD_NOT_ALLOWED: &'static str = "METHOD_NOT_ALLOWED";

#[derive(Debug)]
pub struct MethodNotAllowedError {
    message: String,
}

impl MethodNotAllowedError {
    // todo: use method enum
    pub fn new(method: &str) -> Self {
        Self {
            message: format!("'{}' method not allowed", method),
        }
    }
}

impl Display for MethodNotAllowedError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for MethodNotAllowedError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl<'a> From<MethodNotAllowedError> for Cow<'a, str> {
    fn from(err: MethodNotAllowedError) -> Self {
        Cow::from(err.message)
    }
}

impl<D> Responder<D> for MethodNotAllowedError {
    fn respond<'a>(self, res: Response<'a, D>) -> MiddlewareResult<'a, D> {
        res.send(ErrorResponse::new(
            MethodNotAllowed,
            METHOD_NOT_ALLOWED,
            "method",
            vec![self.description()],
        ))
    }
}
