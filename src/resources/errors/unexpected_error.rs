use hyper::status::StatusCode::InternalServerError;
use nickel::{MiddlewareResult, Responder, Response};
use std::borrow::Cow;
use std::error::Error;
use std::fmt::{self, Display, Formatter};

use super::ErrorResponse;

pub const UNEXPECTED_ERROR: &'static str = "UNEXPECTED_ERROR";

#[derive(Debug)]
pub struct UnexpectedError {
    message: String,
}

impl UnexpectedError {
    pub fn new(reason: Option<&str>) -> Self {
        Self {
            message: match reason {
                Some(r) => format!("Unexpected error: {}", r),
                None => "Unexpected error".to_string(),
            },
        }
    }
}

impl Display for UnexpectedError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for UnexpectedError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl<'a> From<UnexpectedError> for Cow<'a, str> {
    fn from(err: UnexpectedError) -> Self {
        Cow::from(err.message)
    }
}

impl<D> Responder<D> for UnexpectedError {
    fn respond<'a>(self, res: Response<'a, D>) -> MiddlewareResult<'a, D> {
        res.send(ErrorResponse::new(
            InternalServerError,
            UNEXPECTED_ERROR,
            "server",
            vec![self.description()],
        ))
    }
}
