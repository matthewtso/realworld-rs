use hyper::status::StatusCode;
use nickel::{MiddlewareResult, Responder, Response};

#[derive(Debug, Clone)]
pub struct ErrorResponse<'a> {
    pub status_code: StatusCode,
    pub error_code: &'a str,
    pub details_key: &'a str,
    pub details: Vec<&'a str>,
}

impl<'a> ErrorResponse<'a> {
    pub fn new(
        status_code: StatusCode,
        error_code: &'a str,
        details_key: &'a str,
        details: Vec<&'a str>,
    ) -> Self {
        Self {
            status_code,
            error_code,
            details_key,
            details,
        }
    }
}

impl<'s, D> Responder<D> for ErrorResponse<'s> {
    fn respond<'a>(self, mut res: Response<'a, D>) -> MiddlewareResult<'a, D> {
        res.headers_mut().append_raw(
            "Content-Type",
            b"application/json; charset=utf-8".to_vec(),
        );
        res.error(
            self.status_code,
            json!({
                "errors": {
                    self.details_key: self.details,
                }
            }).to_string(),
        )
    }
}
