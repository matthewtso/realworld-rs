use hyper::status::StatusCode::NotFound;
use nickel::{MiddlewareResult, Responder, Response};
use std::borrow::Cow;
use std::error::Error;
use std::fmt::{self, Display, Formatter};

use super::ErrorResponse;

pub const NOT_FOUND: &'static str = "NOT_FOUND";

#[derive(Debug)]
pub struct NotFoundError {
    message: String,
    target: String,
}

impl NotFoundError {
    pub fn body(resource: &str, id: Option<&str>) -> Self {
        Self {
            message: match id {
                Some(id) => format!("Not found: {} {}", resource, id),
                None => format!("Not found: {}", resource),
            },
            target: "body".to_string(),
        }
    }

    pub fn path(resource: &str, id: Option<&str>) -> Self {
        Self {
            message: match id {
                Some(id) => format!("Not found: {} {}", resource, id),
                None => format!("Not found: {}", resource),
            },
            target: "path".to_string(),
        }
    }

    pub fn token(resource: &str, id: Option<&str>) -> Self {
        Self {
            message: match id {
                Some(id) => format!("Not found: {} {}", resource, id),
                None => format!("Not found: {}", resource),
            },
            target: "token".to_string(),
        }
    }
}

impl Display for NotFoundError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for NotFoundError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl<'a> From<NotFoundError> for Cow<'a, str> {
    fn from(err: NotFoundError) -> Self {
        Cow::from(err.message)
    }
}

impl<D> Responder<D> for NotFoundError {
    fn respond<'a>(self, res: Response<'a, D>) -> MiddlewareResult<'a, D> {
        res.send(ErrorResponse::new(
            NotFound,
            NOT_FOUND,
            &self.target,
            vec![self.description()],
        ))
    }
}
