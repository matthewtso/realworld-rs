use hyper::method::Method::{Get, Post, Put};
use nickel::{Middleware, MiddlewareResult, Request, Response};
use serde_json;
use serde_json::value::Value;
use std::collections::HashMap;
use std::error::Error;
use std::io::Read;
use std::sync::Arc;

use super::error_mapper::ErrorMapper;
use super::errors::{
    InvalidInputError, MethodNotAllowedError, NotFoundError, UnexpectedError,
};
use super::helpers::{from_body, method_not_allowed};
use super::JsonResponse;
use helpers::authentication::AuthenticationHelper;
use processors::ProcessorErrorKind;
//use processors::UserProcessor;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ArticleCreateRequest {
    title: String,
    description: String,
    body: String,
    tag_list: Option<Vec<String>>,
}

type ArticleUpdateInner = Value;

#[derive(Debug, Deserialize)]
struct ArticleRequest<T> {
    article: T,
}

#[derive(Debug, Clone)]
pub struct ArticleResource {
    //    profile_processor: Arc<ProfileProcessor>,
//    follow_processor: Arc<FollowProcessor>,
//    auth_helper: AuthenticationHelper,
}

impl<T> Middleware<T> for ArticleResource {
    fn invoke<'a>(
        &self,
        req: &mut Request<T>,
        res: Response<'a, T>,
    ) -> MiddlewareResult<'a, T> {
        match (&req.origin.method, req.param("slug"), req.param("action")) {
            // /articles/:slug

//            (Get, Some(_), None) => self.get_instance(req, res),

            // /articles/:slug/favorite
//            (Post, Some(_), Some("favorite")) => {
//                self.post_instance_follow(req, res)
//            }
//            (Delete, Some(_), Some("favorite")) => {
//                self.delete_instance_follow(req, res)
//            }

            // self.post_instance(req, res), // Method not allowed error (not Not found because we already matched some route)
            _ => method_not_allowed(req, res),
        }
    }
}

impl ArticleResource {
    pub fn post_collection<'mw, D>(
        &self,
        req: &mut Request<D>,
        mut res: Response<'mw, D>,
    ) -> MiddlewareResult<'mw, D> {
        let body: ArticleRequest<Value> = match from_body(req) {
            Ok(body) => body,
            Err(e) => return res.send(e),
        };

        res.send("hi")
    }
}
