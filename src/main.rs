extern crate registry_service;

fn main() {
    let path = std::env::args()
        .nth(1)
        .expect("The first argument should be a path to the yaml configuration file: ./registry_service [path]");

    registry_service::run(path);
}
