extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
extern crate bcrypt;
extern crate jsonwebtoken;
extern crate regex;
extern crate serde_yaml;
extern crate uuid;
#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;
extern crate env_logger;

extern crate chrono;
extern crate hyper;
extern crate nickel;

mod config;
mod externals;
mod helpers;
mod models;
mod processors;
mod resources;

use std::fs::File;
use std::io::Read;
use std::path::Path;

use nickel::{HttpRouter, Nickel, Options};

pub use config::Configuration;

use processors::ProcessorFactory;
use resources::ResourceFactory;

pub fn run<P: AsRef<Path>>(config_path: P) {
    env_logger::Builder::from_default_env()
        .default_format_timestamp(true)
        .init();

    let config: Configuration = read_config_from_yaml_file(config_path);
    debug!("{:#?}", config);

    setup(&config)
        .listen(format!("127.0.0.1:{}", &config.server.http.port))
        .unwrap();
}

pub fn read_config_from_yaml_file<P: AsRef<Path>>(path: P) -> Configuration {
    let mut buf = String::new();
    let mut file = File::open(path).unwrap();
    file.read_to_string(&mut buf).unwrap();
    serde_yaml::from_str(&buf).unwrap()
}

pub fn setup(config: &Configuration) -> Nickel {
    let options = Options::default();
    let mut server = Nickel::with_options(options.output_on_listen(true));

    let resource_factory =
        ResourceFactory::new(ProcessorFactory::new(config), config);

    server.post("/api/users", resource_factory.get_user_resource());
    server.get("/api/user", resource_factory.get_user_resource());
    server.put("/api/user", resource_factory.get_user_resource());
    server.post("/api/users/:path", resource_factory.get_user_resource());

    server.get(
        "/api/profiles/:username",
        resource_factory.get_profile_resource(),
    );
    server.post(
        "/api/profiles/:username/:action",
        resource_factory.get_profile_resource(),
    );
    server.delete(
        "/api/profiles/:username/:action",
        resource_factory.get_profile_resource(),
    );

    server.handle_error(resources::AppErrorHandler);

    server
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
