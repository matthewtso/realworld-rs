mod configuration;

pub use self::configuration::CassandraOrMock;
pub use self::configuration::Configuration;
