#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ServerConfiguration {
    pub http: HttpConfiguration,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct HttpConfiguration {
    pub port: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct HandlerConfiguration {
    pub user_storage: CassandraOrMock,
    pub follow_storage: CassandraOrMock,
    pub article_storage: CassandraOrMock,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum CassandraOrMock {
    Cassandra,
    Mock,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ApplicationConfiguration {
    pub token_secret: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Configuration {
    pub server: ServerConfiguration,
    pub handlers: HandlerConfiguration,
    pub application: ApplicationConfiguration,
}
