use models::User;
use std::fmt::Debug;

pub trait UserStorage: Debug + Sync + Send {
    fn set(&mut self, user: User) -> Option<User>;
    fn get(&self, user_id: &str) -> Option<User>;
    fn get_by_username(&self, username: &str) -> Option<User>;
    fn get_by_email(&self, email: &str) -> Option<User>;
}
