use chrono::{NaiveDateTime, Utc};
use externals::ArticleStorage;
use models::Article;
use std::collections::{BTreeMap, HashSet};

#[derive(Debug, Clone)]
pub struct ArticleStorageMock {
    in_memory_store: BTreeMap<String, Article>,
}

impl ArticleStorageMock {
    pub fn new() -> Self {
        Self {
            in_memory_store: BTreeMap::new(),
        }
    }
}

impl ArticleStorage for ArticleStorageMock {
    fn set(&mut self, mut article: Article) -> Option<Article> {
        article.set_updated_at(Utc::now().naive_utc());
        self.in_memory_store
            .insert(article.get_id().to_string(), article.clone())
            .map(|_| article)
    }

    fn get_by_id(&self, id: &str) -> Option<Article> {
        self.in_memory_store.get(id).map(|article| article.clone())
    }

    fn get_by_slug(&self, slug: &str) -> Option<Article> {
        let slug = slug.to_string();
        for (id, article) in self.in_memory_store.iter() {
            if slug == article.get_slug() {
                return Some(article.clone());
            }
        }
        None
    }

    fn delete(&mut self, id: &str) -> Option<Article> {
        self.in_memory_store.remove(id)
    }

    fn find_by_tag(&self, tag: &str) -> Vec<Article> {
        self.in_memory_store
            .iter()
            .filter(|(id, article)| article.get_tags().contains(tag))
            .map(|(id, article)| article.clone())
            .collect()
    }

    fn find_by_author(&self, author_id: &str) -> Vec<Article> {
        self.in_memory_store
            .iter()
            .filter(|(id, article)| article.get_author_id() == author_id)
            .map(|(id, article)| article.clone())
            .collect()
    }

    fn find_recently_updated(&self, limit: u32, offset: u32) -> Vec<Article> {
        let mut all = self.in_memory_store
            .iter()
            .map(|(k, v)| v.clone())
            .collect::<Vec<Article>>();
        all.sort_by(|a, b| b.get_updated_at().cmp(a.get_updated_at()));
        all
    }
}
