use models::Article;
use std::fmt::Debug;

pub trait ArticleStorage: Debug + Sync + Send {
    fn set(&mut self, article: Article) -> Option<Article>;
    fn get_by_id(&self, id: &str) -> Option<Article>;
    fn get_by_slug(&self, slug: &str) -> Option<Article>;
    fn delete(&mut self, id: &str) -> Option<Article>;
    fn find_by_tag(&self, tag: &str) -> Vec<Article>;
    fn find_by_author(&self, author_id: &str) -> Vec<Article>;
    fn find_recently_updated(&self, limit: u32, offset: u32) -> Vec<Article>;
}
