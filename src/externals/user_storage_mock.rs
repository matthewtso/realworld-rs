use super::UserStorage;
use models::User;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct UserStorageMock {
    in_memory_store: HashMap<String, User>,
}

impl UserStorageMock {
    pub fn new() -> Self {
        Self {
            in_memory_store: HashMap::new(),
        }
    }
}

impl UserStorage for UserStorageMock {
    fn get(&self, user_id: &str) -> Option<User> {
        self.in_memory_store.get(user_id).map(|u| u.clone())
    }

    fn set(&mut self, user: User) -> Option<User> {
        // Will be None on first insert or the old user on subsequent inserts
        // since HashMap returns the old result.
        self.in_memory_store
            .insert(user.get_id().to_string(), user.clone())
            .map(|_| user)
    }

    fn get_by_username(&self, username: &str) -> Option<User> {
        for (_, user) in self.in_memory_store.iter() {
            if user.get_username() == username {
                return Some(user.clone());
            }
        }
        None
    }

    fn get_by_email(&self, email: &str) -> Option<User> {
        for (_, user) in self.in_memory_store.iter() {
            if user.get_email() == email {
                return Some(user.clone());
            }
        }
        None
    }
}
