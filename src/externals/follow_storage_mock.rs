use externals::FollowStorage;
use models::Follow;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct FollowStorageMock {
    in_memory_store: HashMap<String, Follow>,
}

impl FollowStorageMock {
    pub fn new() -> Self {
        Self {
            in_memory_store: HashMap::new(),
        }
    }
}

impl FollowStorage for FollowStorageMock {
    fn set(&mut self, from_id: &str, to_id: &str) -> Option<Follow> {
        let follow_id = [from_id, to_id].join("");
        let follow = Follow::new(from_id.to_string(), to_id.to_string());
        self.in_memory_store.insert(follow_id, follow.clone());
        Some(follow)
    }

    fn get(&self, from_id: &str, to_id: &str) -> Option<Follow> {
        let follow_id = [from_id, to_id].join("");
        self.in_memory_store.get(&follow_id).map(|f| f.clone())
    }

    fn delete(&mut self, from_id: &str, to_id: &str) -> Option<Follow> {
        let follow_id = [from_id, to_id].join("");
        self.in_memory_store.remove(&follow_id)
    }
}
