use models::Follow;
use std::fmt::Debug;

pub trait FollowStorage: Debug + Sync + Send {
    fn set(&mut self, from_id: &str, to_id: &str) -> Option<Follow>;
    fn get(&self, from_id: &str, to_id: &str) -> Option<Follow>;
    fn delete(&mut self, from_id: &str, to_id: &str) -> Option<Follow>;
}
