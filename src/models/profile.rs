use resources::ToJson;
use serde_json::value::Value;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Profile {
    username: String,
    bio: String,
    image: Option<String>,
    following: bool,
}

impl Profile {
    pub fn new(
        username: String,
        bio: String,
        image: Option<String>,
        following: bool,
    ) -> Self {
        Self {
            username,
            bio,
            image,
            following,
        }
    }

    pub fn get_username(&self) -> &str {
        &self.username
    }

    pub fn get_bio(&self) -> &str {
        &self.bio
    }

    pub fn get_image(&self) -> Option<&str> {
        match self.image {
            Some(ref i) => Some(&i),
            None => None,
        }
    }
}

impl ToJson for Profile {
    fn to_json(&self) -> Value {
        json!({
            "profile": {
                "username": self.username,
                "bio": self.bio,
                "image": self.image,
                "following": self.following,
            }
        })
    }
}
