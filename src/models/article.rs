use chrono::NaiveDateTime;
use regex::Regex;
use serde_json::value::Value;
use std::collections::HashSet;

use super::{Profile, TagList};
use resources::ToJson;

lazy_static! {
    static ref WHITESPACE: Regex = Regex::new(r"\s").unwrap();
}

#[derive(Debug, Clone)]
//#[serde(rename_all = "camelCase")] -> needs Ser and De derives
pub struct Article {
    id: String,
    title: String,
    description: String,
    body: String,

    tag_list: TagList,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
    author_id: String,

    favorites: HashSet<String>, // Set of user IDs that have favorited this article.
                                //    favorited: bool,
                                //    favorites_count: u32,
}

impl Article {
    pub fn new(
        id: String,
        title: String,
        description: String,
        body: String,

        tag_list: TagList,
        created_at: NaiveDateTime,
        updated_at: NaiveDateTime,
        author_id: String,
        favorites: HashSet<String>,
    ) -> Self {
        Self {
            id,
            title,
            description,
            body,
            tag_list,
            created_at,
            updated_at,
            author_id,
            favorites,
        }
    }

    pub fn get_id(&self) -> &str {
        self.id.as_str()
    }

    pub fn get_author_id(&self) -> &str {
        self.author_id.as_str()
    }

    pub fn title_to_slug(title: &str) -> String {
        WHITESPACE.replace_all(title, "-").to_lowercase()
    }

    pub fn get_updated_at(&self) -> &NaiveDateTime {
        &self.updated_at
    }

    pub fn set_updated_at(&mut self, updated_at: NaiveDateTime) {
        self.updated_at = updated_at;
    }

    pub fn get_slug(&self) -> String {
        Article::title_to_slug(self.title.as_str())
    }

    pub fn get_tags(&self) -> &TagList {
        &self.tag_list
    }

    //    fn make_external_article(article: Article, author: Profile)

    pub fn to_json(&self) -> Value {
        json!({
            "slug": self.get_slug(),
            "title": &self.title,
            "description": &self.description,
            "body": &self.body,
            "tagList": self.tag_list.to_json(),
            "createdAt": &self.created_at,
            "updatedAt": &self.updated_at,
            "favorited": false,
            "favoritesCount": &self.favorites.len(),
//            "author": self.author.to_json(),
        })
    }
}
