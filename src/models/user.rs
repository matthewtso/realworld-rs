use bcrypt::{hash, verify, DEFAULT_COST};
use resources::errors::UnexpectedError;
use serde_json;
use serde_json::value::Value;
use std::error::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    #[serde(skip_serializing, skip_deserializing)]
    id: String,
    username: String,
    email: String,
    bio: String,
    image: Option<String>,
    #[serde(skip_serializing, skip_deserializing)]
    password_hash: String,
    // #[serde(skip_serializing, skip_deserializing)]
    // created_at: NaiveDate,
}

impl User {
    pub fn new(
        id: String,
        username: String,
        email: String,
        bio: String,
        image: Option<String>,
        password_hash: String,
    ) -> Self {
        Self {
            id,
            username,
            email,
            bio,
            image,
            password_hash,
        }
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    pub fn to_json_with_token(&self, token: &str) -> Value {
        json!({
            "user": {
                "email": self.email,
                "token": token,
                "username": self.username,
                "bio": self.bio,
                "image": self.image,
            }
        })
    }

    pub fn get_username(&self) -> &str {
        &self.username
    }

    pub fn set_username(&mut self, username: String) {
        self.username = username;
    }

    pub fn get_email(&self) -> &str {
        &self.email
    }

    pub fn set_email(&mut self, email: String) {
        self.email = email;
    }

    pub fn get_bio(&self) -> &str {
        &self.bio
    }

    pub fn set_bio(&mut self, bio: String) {
        self.bio = bio;
    }

    pub fn get_image(&self) -> Option<&str> {
        match self.image {
            Some(ref i) => Some(&i),
            None => None,
        }
    }

    pub fn set_image(&mut self, image: Option<String>) {
        self.image = image;
    }

    pub fn get_password_hash(&self) -> &str {
        &self.password_hash
    }

    // TODO: Use processor error instead?
    pub fn set_password(
        &mut self,
        password: &str,
    ) -> Result<(), UnexpectedError> {
        let hashed = match hash(password, 10) {
            //DEFAULT_COST) {
            Ok(h) => h,
            Err(e) => return Err(UnexpectedError::new(Some(e.description()))),
        };

        self.password_hash = hashed;
        Ok(())
    }

    pub fn verify_password(&self, password: &str) -> bool {
        match verify(password, self.get_password_hash()) {
            Ok(v) => v,
            Err(e) => {
                error!("Password verify error: {}", e);
                false
            }
        }
    }
}
