use super::User;
use chrono::NaiveDate;

#[derive(Debug, Clone)]
//#[serde(rename_all = "camelCase")] -> needs Ser and De derives
pub struct Comment {
    id: String,
    created_at: NaiveDate,
    updated_at: NaiveDate,
    body: String,
    author: User,
}

impl Comment {}
