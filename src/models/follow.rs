#[derive(Debug, Clone)]
pub struct Follow {
    from_id: String,
    to_id: String,
}

impl Follow {
    pub fn new(from_id: String, to_id: String) -> Self {
        Self { from_id, to_id }
    }

    pub fn get_from_id(&self) -> &str {
        &self.from_id
    }

    pub fn get_to_id(&self) -> &str {
        &self.to_id
    }
}
