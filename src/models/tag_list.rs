use serde_json::value::Value;
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct TagList {
    tags: HashSet<String>,
}

impl TagList {
    pub fn new(tags: HashSet<String>) -> Self {
        Self { tags }
    }

    pub fn to_json(&self) -> Value {
        json!(self.tags)
    }

    pub fn contains(&self, tag: &str) -> bool {
        self.tags.contains(&tag.to_string())
    }
}
