use super::Claims;
use jsonwebtoken::{decode, encode, Header, Validation};
use nickel::Request;
use resources::errors::{InvalidInputError, UnexpectedError};

#[derive(Debug, Clone)]
pub struct AuthenticationHelper {
    token_secret: String,
    validation: Validation,
}

impl AuthenticationHelper {
    pub fn new(token_secret: String) -> Self {
        let mut validation = Validation::default();
        validation.validate_exp = false;
        Self {
            token_secret,
            validation,
        }
    }

    pub fn encode_token(
        &self,
        user_id: &str,
    ) -> Result<String, UnexpectedError> {
        match encode(
            &Header::default(),
            &Claims {
                user_id: user_id.to_string(),
                exp: 0,
            },
            &self.token_secret.as_bytes(),
        ) {
            Ok(token) => Ok(token),
            Err(e) => Err(UnexpectedError::new(Some(&format!("{}", e)))),
        }
    }

    pub fn get_auth_from_request<D>(
        &self,
        req: &Request<D>,
    ) -> Result<Claims, InvalidInputError> {
        self.get_auth_from_headers(req.origin.headers.get_raw("authorization"))
    }

    fn get_auth_from_headers(
        &self,
        headers: Option<&[Vec<u8>]>,
    ) -> Result<Claims, InvalidInputError> {
        if let Some(auths) = headers {
            for auth in auths {
                if let Ok(authstring) = String::from_utf8(auth.to_vec()) {
                    let mut pieces = authstring.split(" ");
                    match (pieces.nth(0), pieces.nth(0)) {
                        (Some("Token"), Some(tokenstring)) => {
                            return self.decode_token(tokenstring)
                        }
                        _ => (),
                    };
                }
            }
        }
        Err(InvalidInputError::header("Missing token"))
    }

    fn decode_token(&self, token: &str) -> Result<Claims, InvalidInputError> {
        let mut validation = Validation::default();
        validation.validate_exp = false;
        match decode::<Claims>(token, self.token_secret.as_ref(), &validation) {
            Ok(tokendata) => Ok(tokendata.claims),
            Err(e) => Err(InvalidInputError::header(&format!("{}", e))),
        }
    }
}
