use std::error::Error as StdError;
use std::fmt;

#[derive(Debug, Clone)]
pub struct ProcessorError(Box<ProcessorErrorKind>);

pub type ProcessorResult<T> = Result<T, ProcessorError>;

#[derive(Debug, Clone)]
pub enum ProcessorErrorKind {
    UserAlreadyExists(String),
    UserNotFound(String),
    TitleAlreadyExists,
    ArticleNotFound,
    FollowNotFound,
    UnexpectedError,
}

impl ProcessorError {
    pub(crate) fn new(error_kind: ProcessorErrorKind) -> Self {
        ProcessorError(Box::new(error_kind))
    }

    /// Return the specific type of this error.
    pub fn kind(&self) -> &ProcessorErrorKind {
        &self.0
    }

    /// Unwrap this error into its underlying type.
    pub fn into_kind(self) -> ProcessorErrorKind {
        *self.0
    }
}

impl StdError for ProcessorError {
    fn description(&self) -> &str {
        match *self.0 {
            ProcessorErrorKind::UserAlreadyExists(ref description) => {
                &description
            }
            ProcessorErrorKind::UserNotFound(ref description) => &description,
            ProcessorErrorKind::FollowNotFound => "Follow not found",
            ProcessorErrorKind::ArticleNotFound => "Article not found",
            ProcessorErrorKind::TitleAlreadyExists => "Title already exists",
            ProcessorErrorKind::UnexpectedError => "Unexpected error",
        }
    }

    fn cause(&self) -> Option<&StdError> {
        match *self.0 {
            ProcessorErrorKind::UserAlreadyExists(_) => None,
            ProcessorErrorKind::UserNotFound(_) => None,
            ProcessorErrorKind::FollowNotFound => None,
            ProcessorErrorKind::ArticleNotFound => None,
            ProcessorErrorKind::TitleAlreadyExists => None,
            ProcessorErrorKind::UnexpectedError => None,
        }
    }
}

impl fmt::Display for ProcessorError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self.0 {
            ProcessorErrorKind::UserAlreadyExists(ref description) => {
                write!(f, "{}", &description)
            }
            ProcessorErrorKind::UserNotFound(ref description) => {
                write!(f, "{}", &description)
            }
            ProcessorErrorKind::FollowNotFound
            | ProcessorErrorKind::ArticleNotFound
            | ProcessorErrorKind::TitleAlreadyExists
            | ProcessorErrorKind::UnexpectedError => {
                write!(f, "{}", self.description())
            }
        }
    }
}

impl From<ProcessorErrorKind> for ProcessorError {
    fn from(error_kind: ProcessorErrorKind) -> ProcessorError {
        ProcessorError::new(error_kind)
    }
}
