use std::sync::{Arc, RwLock};

use super::{ProcessorError, ProcessorErrorKind, ProcessorResult};
use externals::FollowStorage;
use models::Follow;
use processors::UserProcessor;

#[derive(Debug, Clone)]
pub struct FollowProcessor {
    follow_storage: Arc<RwLock<FollowStorage>>,
    user_processor: Arc<UserProcessor>,
}

impl FollowProcessor {
    pub fn new(
        follow_storage: Arc<RwLock<FollowStorage>>,
        user_processor: Arc<UserProcessor>,
    ) -> Self {
        Self {
            follow_storage,
            user_processor,
        }
    }

    pub fn is_following(&self, from_id: &str, to_id: &str) -> bool {
        self.follow_storage
            .read()
            .unwrap()
            .get(from_id, to_id)
            .map(|_| true)
            .unwrap_or(false)
    }

    pub fn create_follow(
        &self,
        from_id: &str,
        to_username: &str,
    ) -> ProcessorResult<Follow> {
        let from_user = self.user_processor.get_by_id(from_id)?;
        let to_user = self.user_processor.get_by_username(to_username)?;

        match self
            .follow_storage
            .write()
            .unwrap()
            .set(from_user.get_id(), to_user.get_id())
        {
            Some(Follow) => Ok(Follow),
            None => {
                Err(ProcessorError::new(ProcessorErrorKind::UnexpectedError))
            }
        }
    }

    /// Returns the deleted Follow
    pub fn delete_follow(
        &self,
        from_id: &str,
        to_username: &str,
    ) -> ProcessorResult<Follow> {
        let from_user = self.user_processor.get_by_id(from_id)?;
        let to_user = self.user_processor.get_by_username(to_username)?;

        match self
            .follow_storage
            .write()
            .unwrap()
            .delete(from_user.get_id(), to_user.get_id())
        {
            Some(Follow) => Ok(Follow),
            None => {
                Err(ProcessorError::new(ProcessorErrorKind::FollowNotFound))
            }
        }
    }
}
