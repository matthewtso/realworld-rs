use super::ProcessorResult;
use models::{Follow, Profile};
use processors::{FollowProcessor, UserProcessor};

use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct ProfileProcessor {
    user_processor: Arc<UserProcessor>,
    follow_processor: Arc<FollowProcessor>,
}

impl ProfileProcessor {
    pub fn new(
        user_processor: Arc<UserProcessor>,
        follow_processor: Arc<FollowProcessor>,
    ) -> Self {
        Self {
            user_processor,
            follow_processor,
        }
    }

    pub fn get_by_username(
        &self,
        username: &str,
        from_id: Option<String>,
    ) -> ProcessorResult<Profile> {
        let user = self.user_processor.get_by_username(username)?;
        Ok(Profile::new(
            user.get_username().to_string(),
            user.get_bio().to_string(),
            user.get_image().map(|i| i.to_string()),
            if let Some(from_id) = from_id {
                self.follow_processor.is_following(&from_id, user.get_id())
            } else {
                false
            },
        ))
    }

    /// Returns the deleted Follow
    pub fn delete_follow_returning_profile(
        &self,
        from_id: &str,
        to_username: &str,
    ) -> ProcessorResult<Profile> {
        self.follow_processor
            .delete_follow(from_id, to_username)
            .and_then(|_| {
                self.get_by_username(to_username, Some(from_id.to_string()))
            }).or_else(|_| {
                self.get_by_username(to_username, Some(from_id.to_string()))
            })
    }
}
