use super::{ProcessorError, ProcessorErrorKind, ProcessorResult};
use externals::UserStorage;
use models::User;

use bcrypt::{hash, DEFAULT_COST};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct UserProcessor {
    user_storage: Arc<RwLock<UserStorage>>,
}

impl UserProcessor {
    pub fn new(user_storage: Arc<RwLock<UserStorage>>) -> Self {
        Self { user_storage }
    }

    pub fn create(
        &self,
        email: &str,
        username: &str,
        password: &str,
        bio: Option<&str>,
        image: Option<&str>,
    ) -> ProcessorResult<User> {
        if let Some(_user) = self.get_by_email(email) {
            return Err(ProcessorError::from(
                ProcessorErrorKind::UserAlreadyExists(format!(
                    "User {} already exists",
                    email
                )),
            ));
        }

        let id = format!("{}", Uuid::new_v4());
        println!("New user ID: {}", id);
        let hashed = match hash(password, DEFAULT_COST) {
            Ok(h) => h,
            Err(e) => {
                println!("{}", e);
                "".to_string()
            }
        };
        println!("New password hash: {}", hashed);
        let user = User::new(
            id,
            username.to_string(),
            email.to_string(),
            if let Some(bio) = bio {
                bio.to_string()
            } else {
                String::new()
            },
            image.map(|s| s.to_string()),
            hashed.to_string(),
        );

        self.user_storage
            .write()
            .unwrap()
            .set(user)
            .ok_or(ProcessorError::from(ProcessorErrorKind::UnexpectedError))
    }

    pub fn update(
        &self,
        user_id: &str,
        fields: HashMap<String, Option<String>>,
    ) -> ProcessorResult<User> {
        let mut user_storage = self.user_storage.write().unwrap();

        let mut user = match user_storage.get(user_id) {
            Some(user) => user,
            None => {
                return Err(ProcessorError::from(
                    ProcessorErrorKind::UserNotFound(format!("User not found")),
                ))
            }
        };

        for (k, v) in fields.iter() {
            match (k.as_str(), v) {
                ("email", Some(email)) => user.set_email(email.to_string()),
                ("username", Some(username)) => {
                    user.set_username(username.to_string())
                }
                ("password", Some(password)) => {
                    user.set_password(password).unwrap()
                }
                ("bio", Some(bio)) => user.set_bio(bio.to_string()),
                ("image", image) => {
                    user.set_image(if let Some(image) = image {
                        Some(image.to_string())
                    } else {
                        None
                    })
                }
                (&_, _) => (),
            };
        }

        user_storage
            .set(user)
            .ok_or(ProcessorError::from(ProcessorErrorKind::UnexpectedError))
    }

    pub fn get_by_id(&self, id: &str) -> ProcessorResult<User> {
        self.user_storage
            .read()
            .unwrap()
            .get(id)
            .ok_or(ProcessorError::new(ProcessorErrorKind::UserNotFound(
                id.to_string(),
            )))
    }

    pub fn get_by_username(&self, username: &str) -> ProcessorResult<User> {
        self.user_storage
            .read()
            .unwrap()
            .get_by_username(username)
            .ok_or(ProcessorError::new(ProcessorErrorKind::UserNotFound(
                username.to_string(),
            )))
    }

    pub fn get_by_email(&self, username: &str) -> Option<User> {
        self.user_storage.read().unwrap().get_by_email(username)
    }

    // TODO: Handle better if the error is not due to mis-matching password (like user not found?).
    pub fn verify_password(&self, username: &str, password: &str) -> bool {
        self.get_by_username(username)
            .map(|user| user.verify_password(password))
            .unwrap_or(false)
    }
}
