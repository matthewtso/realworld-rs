use super::{ProcessorError, ProcessorErrorKind, ProcessorResult};
use externals::ArticleStorage;
use models::{Article, TagList};

use bcrypt::{hash, DEFAULT_COST};
use chrono::Utc;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock};
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct ArticleProcessor {
    article_storage: Arc<RwLock<ArticleStorage>>,
}

impl ArticleProcessor {
    pub fn new(article_storage: Arc<RwLock<ArticleStorage>>) -> Self {
        Self { article_storage }
    }

    pub fn get_by_slug(&self, slug: &str) -> ProcessorResult<Article> {
        self.article_storage
            .read()
            .unwrap()
            .get_by_slug(slug)
            .ok_or(ProcessorError::new(ProcessorErrorKind::ArticleNotFound))
    }

    pub fn create(
        &self,
        author_id: &str,
        title: &str,
        description: &str,
        body: &str,
        tag_list: HashSet<String>,
    ) -> ProcessorResult<Article> {
        if let Ok(_) = self.get_by_slug(&Article::title_to_slug(title)) {
            return Err(ProcessorError::new(
                ProcessorErrorKind::TitleAlreadyExists,
            ));
        }

        let article = Article::new(
            format!("{}", Uuid::new_v4()),
            title.to_string(),
            description.to_string(),
            body.to_string(),
            TagList::new(tag_list),
            Utc::now().naive_utc(),
            Utc::now().naive_utc(),
            author_id.to_string(),
            HashSet::new(),
        );

        self.article_storage
            .write()
            .unwrap()
            .set(article)
            .ok_or(ProcessorError::new(ProcessorErrorKind::UnexpectedError))
    }

    // pub fn update(&self, fields: HashMap<String, String>
    // pub fn delete(&self, slug: &str) -> ProcessorResult<Article>
    // pub fn get_by_id(&self, id: &str) -> ProcessorResult<Article>
    // pub fn get_by_slug(&self, slug: &str) -> ProcessorResult<Article>
    // pub fn favorite(&self, user_id: &str) -> ProcessorResult<Article>
    // pub fn unfavorite(&self, user_id: &str) -> ProcessorResult<Article>
    // pub fn find_by_tag(&self, tag: &str, limit: u32, offset: u32) -> ProcessorResult<Vec<Article>>
    // pub fn find_by_author(&self, tag: &str, limit: u32, offset: u32) -> ProcessorResult<Vec<Article>>
}
