use std::sync::Arc;
use std::sync::RwLock;

use super::{
    ArticleProcessor, FollowProcessor, ProfileProcessor, UserProcessor,
};
use config::CassandraOrMock::{Cassandra, Mock};
use config::Configuration;
use externals::{ArticleStorageMock, FollowStorageMock, UserStorageMock};

#[derive(Debug, Clone)]
pub struct ProcessorFactory {
    user_processor: UserProcessor,
    profile_processor: ProfileProcessor,
    follow_processor: FollowProcessor,
    article_processor: ArticleProcessor,
}

impl ProcessorFactory {
    pub fn new(configuration: &Configuration) -> Self {
        let user_storage = match configuration.handlers.user_storage {
            Cassandra => {
                panic!(format!("{:?} user storage not implemented", Cassandra))
            }
            Mock => Arc::new(RwLock::new(UserStorageMock::new())),
        };
        let follow_storage = match configuration.handlers.follow_storage {
            Cassandra => panic!(format!(
                "{:?} follow storage not implemented",
                Cassandra
            )),
            Mock => Arc::new(RwLock::new(FollowStorageMock::new())),
        };
        let article_storage = match configuration.handlers.follow_storage {
            Cassandra => panic!(format!(
                "{:?} article storage not implemented",
                Cassandra
            )),
            Mock => Arc::new(RwLock::new(ArticleStorageMock::new())),
        };

        let user_processor = UserProcessor::new(user_storage.clone());
        let follow_processor = FollowProcessor::new(
            follow_storage.clone(),
            Arc::new(user_processor.clone()),
        );

        let profile_processor = ProfileProcessor::new(
            Arc::new(user_processor.clone()),
            Arc::new(follow_processor.clone()),
        );

        let article_processor = ArticleProcessor::new(article_storage.clone());

        Self {
            user_processor,
            profile_processor,
            follow_processor,
            article_processor,
        }
    }

    pub fn get_user_processor(&self) -> UserProcessor {
        self.user_processor.clone()
    }

    pub fn get_profile_processor(&self) -> ProfileProcessor {
        self.profile_processor.clone()
    }

    pub fn get_follow_processor(&self) -> FollowProcessor {
        self.follow_processor.clone()
    }

    pub fn get_article_processor(&self) -> ArticleProcessor {
        self.article_processor.clone()
    }
}
