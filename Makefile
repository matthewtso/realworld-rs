.PHONY: build

config_path = config/registry_service.yml

build-debug:
	cargo build

run: build-debug
	RUST_LOG=api_service=info ./target/debug/registry_service $(config_path)

run-debug: build-debug
	RUST_LOG=api_service=debug ./target/debug/registry_service $(config_path)
